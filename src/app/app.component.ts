import { Component, OnInit } from '@angular/core';
import { of, Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { MainService } from './main.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public apiObject: any;
  public inFlight$;
  githubObject: Observable<object>;

  constructor(
    private readonly http: HttpClient,
    private readonly mainService: MainService
  ) { }

  ngOnInit(): void {
    this.getOrganisationUrl();

    // setTimeout(() => {
    this.getFollowers();
    this.getName();
    // }, 1000);
  }

  getOrganisationUrl(): Observable<any> {
    return this.getGithubObject().pipe(map((apiObject: any) => apiObject.organizations_url));
  }

  getName(): Observable<any> {
    return this.getGithubObject().pipe(map((object: any) => object.name));
  }

  getFollowers(): Observable<any> {
    return this.getGithubObject().pipe(map((apiObject: any) => apiObject.followers_url));
  }

  getGithubObject(): Observable<object> {
    // No request made - make the request (3)


    // Request in progress - wait for the in flight request to return (2)


    // Request made and value returned - so return the value (1)


    // Returns the JSON object from Github and saves it locally when returned

    if (this.apiObject) { // (1)
      return of(this.apiObject);
    }

    if (this.inFlight$) { // (2)
      return this.inFlight$;
    }

    this.inFlight$ = this.http.get('https://api.github.com/users/james').pipe(tap(obj => this.apiObject = obj)); // (3)

    return this.inFlight$;

    //   console.log('getGithubObject ran');
    //   if (this.apiObject) {
    //     return of(this.apiObject);
    //   } else {
    //     return this.http.get('https://api.github.com/users/james').pipe(tap(obj => this.apiObject = obj));
    //   }
  }
}
